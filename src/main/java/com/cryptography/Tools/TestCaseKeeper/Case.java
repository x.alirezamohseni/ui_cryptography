package com.cryptography.Tools.TestCaseKeeper;

import java.util.Map;

public class Case
{
    private final String key;
    private final String originalMessage;
    private final String encryptedMessage;
    private final Map<String, Integer> doubleRepetitions;

    public String getKey()
    {
        return key;
    }

    public String getOriginalMessage()
    {
        return originalMessage;
    }

    public String getEncryptedMessage()
    {
        return encryptedMessage;
    }

    public Map<String, Integer> getDoubleRepetitions()
    {
        return doubleRepetitions;
    }

    public Case(String originalMessage, String encryptedMessage, Map<String, Integer> doubleRepetitions, String key)
    {
        this.originalMessage = originalMessage;
        this.encryptedMessage = encryptedMessage;
        this.doubleRepetitions = doubleRepetitions;
        this.key = key;
    }
}
