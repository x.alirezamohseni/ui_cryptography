package com.cryptography.Tools.FileHandler;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandler
{
    private final String baseDir;

    public FileHandler(String baseDir)
    {
        this.baseDir = baseDir;
    }

    private boolean isPathExist(String path)
    {
        final File pathToDestination = new File(path);

        return pathToDestination.exists();

    }

    public boolean makeBaseDirectory()
    {
        try
        {
            File baseDir = new File(this.baseDir);
            if (!baseDir.exists() && !baseDir.isDirectory())
            {
                return baseDir.mkdir();
            }
        } catch (Exception e)
        {
            return false;
        }
        return false;
    }

    public boolean makeAndWriteIntoFile(String dir, String fileName, String content)
    {
        String fileLocationDir = this.baseDir + "/" + dir;
        new File(fileLocationDir).mkdirs();
        String fileLocation = fileLocationDir + "/" + fileName;
        File file = new File(fileLocation);
        if (!file.isFile() && !file.exists())
        {
            try
            {
                if (file.createNewFile())
                {
                    try (FileWriter writer = new FileWriter(file))
                    {
                        writer.write(content);
                        writer.close();
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    return true;
                }
                else {return false;}

            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return false;
    }
}
