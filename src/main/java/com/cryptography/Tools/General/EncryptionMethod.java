package com.cryptography.Tools.General;

public enum EncryptionMethod
{
    Caesar,
    Hill,
    Vigenere
}
