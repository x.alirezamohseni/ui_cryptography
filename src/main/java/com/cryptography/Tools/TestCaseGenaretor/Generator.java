package com.cryptography.Tools.TestCaseGenaretor;

import com.cryptography.Tools.FileHandler.FileHandler;
import com.cryptography.Tools.TestCaseKeeper.*;
import com.cryptography.Tools.General.EncryptionMethod;
import com.cryptography.Tools.WordProcessor.WordProcessor;
import com.cryptography.classic.symmetric.caesar.Caesar;
import com.cryptography.classic.symmetric.hill.*;
import com.cryptography.classic.symmetric.vigenere.Vigenere;

import java.io.*;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

public class Generator
{
    public Case[] generate(String baseDir, String pathOfOriginalText, int countOfTestCases, int leastCharactersPerEachTestCase, EncryptionMethod encryptionMethod)
    {
        if (!checkArgs(baseDir, pathOfOriginalText, countOfTestCases, leastCharactersPerEachTestCase))
            return null;
        String[] parts = new WordProcessor().stringDivider(new WordProcessor().clearString(Objects.requireNonNull(readFile(pathOfOriginalText))),countOfTestCases, leastCharactersPerEachTestCase);
        Case[] cases = new Case[countOfTestCases];
        switch (encryptionMethod)
        {
            case Hill ->
            {
                hillEnc(cases, parts);
            }
            case Caesar ->
            {
                caesarEnc(cases, parts);
            }
            case Vigenere ->
            {
                vigenereEnc(cases, parts);
            }
        }
        return cases;
    }

    private void vigenereEnc(Case[] cases, String[] parts)
    {
        if (cases.length != parts.length)
            return;
        for (int i = 0; i < cases.length; i++)
        {
            WordProcessor wordProcessor = new WordProcessor();
            Vigenere vigenere = new Vigenere();
            String rawKey = generateRandomVigenereKey(parts[i].length());
            String key = vigenere.fitKeyToMessage(rawKey, parts[i].length());
            String enc = Vigenere.encrypt(parts[i], key);
            cases[i] = new Case(parts[i], enc, wordProcessor.getDoubleRepetitions(parts[i]), key);
        }
    }

    private String generateRandomVigenereKey(int length)
    {
        String alphabets = "abcdefghijklmnopqrstuvwxyz";
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length+5; i++)
        {
            String chosen = String.valueOf(alphabets.charAt(new Random().nextInt(26)));
            builder.append(chosen);
        }
        return builder.toString();
    }

    private void caesarEnc(Case[] cases, String[] parts)
    {
        if (cases.length != parts.length)
            return;
        for (int i = 0; i < cases.length; i++)
        {
            WordProcessor wordProcessor = new WordProcessor();
            Caesar caesar = new Caesar();
            caesar.setNewRandomKey();
            int key = caesar.getKey();
            String enc = caesar.encrypt(parts[i]);
            cases[i] = new Case(parts[i], enc, wordProcessor.getDoubleRepetitions(parts[i]), Integer.toString(key));
        }
    }

    public void writeToFile(Case[] cases, String basePath, String method)
    {
        basePath += String.format("/%s", method);
        FileHandler handler = new FileHandler(basePath);
        for (int i = 0; i < cases.length; i++)
        {
            handler.makeBaseDirectory();
            handler.makeAndWriteIntoFile(String.format("/TestCase_%d.d", i),"original",cases[i].getOriginalMessage());
            handler.makeAndWriteIntoFile(String.format("/TestCase_%d.d", i),"encrypted",cases[i].getEncryptedMessage());
            handler.makeAndWriteIntoFile(String.format("/TestCase_%d.d", i),"key",cases[i].getKey());
            handler.makeAndWriteIntoFile(String.format("/TestCase_%d.d", i),"rep", mapToStr(cases[i].getDoubleRepetitions()));

        }
    }

    private boolean checkArgs(String baseDir, String pathOfOriginalText, int countOfTestCases, int leastCharactersPerEachTestCase)
    {
        WordProcessor wordProcessor = new WordProcessor();
        String originalText = readFile(pathOfOriginalText);
        try
        {
            wordProcessor.stringDivider(originalText, countOfTestCases, leastCharactersPerEachTestCase);

        }
        catch (IllegalArgumentException e)
        {
            return false;
        }
        return true;
    }

    private String readFile(String path)
    {
        try
        {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            StringBuilder builder = new StringBuilder();
            String string;
            while ((string = bufferedReader.readLine()) != null)
            {
                builder.append(string);
            }
            string = builder.toString();
            return string;
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void hillEnc(Case[] cases, String[] parts)
    {
        if (cases.length != parts.length)
            return;
        for (int i = 0; i < cases.length; i++)
        {
            WordProcessor wordProcessor = new WordProcessor();
            HillMatrix key =  generateRandomHillMatrix();
            String enc = Hill.encrypt(key,parts[i]);
            cases[i] = new Case(parts[i], enc, wordProcessor.getDoubleRepetitions(parts[i]), key.toString());
        }
    }

    private HillMatrix generateRandomHillMatrix()
    {
        int a, b, c, d;
        int[][] init = new int[2][2];
        do
        {
            Random random = new Random();
            a = random.nextInt(25);
            b = random.nextInt(25);
            c = random.nextInt(25);
            d = random.nextInt(25);

        }
        while ((a*d - b*c) ==0);
        init[0][0] = a;
        init[0][1] = b;
        init[1][0] = c;
        init[1][1] = d;
        return new HillMatrix(init);
    }

    private String mapToStr(Map<String, Integer> map)
    {
        StringBuilder doubleRepkeaper = new StringBuilder();
        map.forEach((key, value) -> doubleRepkeaper.append(key).append(" -> ").append(value).append("\n"));
        return doubleRepkeaper.toString();
    }
}
