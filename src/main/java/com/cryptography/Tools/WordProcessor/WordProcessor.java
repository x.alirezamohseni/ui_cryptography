package com.cryptography.Tools.WordProcessor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class WordProcessor
{

    public String clearString(String text)
    {
        StringBuilder clearedString = new StringBuilder();
        char[] textCharacters = text.toCharArray();
        for (char character : textCharacters)
        {
            if ((int) character >= 65 && (int) character <= 90)
                clearedString.append(Character.toLowerCase(character));
            if ((int) character >= 97 && (int) character <= 122)
                clearedString.append(Character.toLowerCase(character));
        }
        return clearedString.toString();
    }

    public Map<String, Integer> getDoubleRepetitions(String string)
    {
        Map<String, Integer> numbersOfRepetitions = new HashMap<>();

        char[] stringCharacters = string.toCharArray();

        for (int i = 0; i < stringCharacters.length - 1; i++)
        {
            String key = String.valueOf(stringCharacters[i]) + stringCharacters[i + 1];
            int countOfRepetitionsOfKey = findRepetitions(string, key);
            numbersOfRepetitions.put(key, countOfRepetitionsOfKey);
        }

        return numbersOfRepetitions;
    }

    public int findRepetitions(String string, String key)
    {
        int countOfRepetitions = 0;
        String tempString = string;
        while (tempString.contains(key))
        {
            tempString = tempString.replaceFirst(key, "");
            ++countOfRepetitions;
        }
        return countOfRepetitions;
    }


    public String[] stringDivider(String string, int countOfDivisions, int leastCharactersPerEachPart)
    {
        if ((string.length() / countOfDivisions) < leastCharactersPerEachPart)
        {
            throw new IllegalArgumentException(String.format("I can not divide the string by %d parts with %d characters in each part.",
                    countOfDivisions, leastCharactersPerEachPart));
        }

        String currentString = string;
        ArrayList<String> parts = new ArrayList<>();
        int range = string.length() / countOfDivisions;
        for (int i = 0; i < countOfDivisions; i++)
        {
            if (i == countOfDivisions-1)
            {
                parts.add(currentString);
            }
            else
            {
                String partToAdd = currentString.substring(0, range);
                currentString = currentString.substring(range);
                parts.add(partToAdd);
            }
        }
        return parts.toArray(new String[0]);
    }
}
