package com.cryptography;

import com.cryptography.Tools.FileHandler.FileHandler;
import com.cryptography.Tools.General.EncryptionMethod;
import com.cryptography.Tools.TestCaseGenaretor.Generator;
import com.cryptography.Tools.TestCaseKeeper.Case;
import com.cryptography.classic.symmetric.caesar.Caesar;
import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.gui2.*;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

import com.cryptography.Tools.WordProcessor.WordProcessor;
import org.w3c.dom.ls.LSOutput;

/**
 * The classic cryptography package.
 *
 * @author AliReza Mohseni
 * @version 0.0.1-SNAPSHOT
 */
public class Main
{

    public static TextBox basePathTxt = new TextBox();
    public static TextBox pathToTextFileTxt = new TextBox();
    public static TextBox countOfTestCases = new TextBox();
    public static TextBox leastCharsPerPart = new TextBox();
    public static ComboBox<String> encryptionMethodsCombo = new ComboBox<>("Caesar", "Hill", "Vigenere");
    public static void main(String[] args) throws IOException
    {

        Main mainObj = new Main();

        Terminal terminal = new DefaultTerminalFactory().createTerminal();

        Screen screen = new TerminalScreen(terminal);
        screen.startScreen();
        MultiWindowTextGUI textGUI = new MultiWindowTextGUI(screen);
        textGUI.setEOFWhenNoWindows(true);
        try
        {

            basePathTxt.setPreferredSize(new TerminalSize(50, 1));
            pathToTextFileTxt.setPreferredSize(new TerminalSize(50, 1));
            countOfTestCases.setPreferredSize(new TerminalSize(50, 1));
            leastCharsPerPart.setPreferredSize(new TerminalSize(50, 1));

            final BasicWindow window = new BasicWindow("Cryptography TestCase Generator");
            window.setFixedSize(new TerminalSize(60, 70));
            Panel contentArea = new Panel();
            contentArea.setPreferredSize(new TerminalSize(90, 100));
            contentArea.setPosition(new TerminalPosition(2000, 3000));
            contentArea.setLayoutManager(new LinearLayout(Direction.VERTICAL));

            contentArea.addComponent(new Label("Enter the base path"));
            contentArea.addComponent(basePathTxt);
            contentArea.addComponent(new Separator(Direction.HORIZONTAL).setPreferredSize(new TerminalSize(60, 1)));

            contentArea.addComponent(new Label("Enter the path of original text file for encoding"));
            contentArea.addComponent(pathToTextFileTxt);
            contentArea.addComponent(new Separator(Direction.HORIZONTAL).setPreferredSize(new TerminalSize(60, 1)));

            contentArea.addComponent(new Label("Enter the number of test cases"));
            contentArea.addComponent(countOfTestCases);
            contentArea.addComponent(new Separator(Direction.HORIZONTAL).setPreferredSize(new TerminalSize(60, 1)));

            contentArea.addComponent(new Label("Enter the minimum characters per each test case"));
            contentArea.addComponent(leastCharsPerPart);
            contentArea.addComponent(new Separator(Direction.HORIZONTAL).setPreferredSize(new TerminalSize(60, 1)));

            contentArea.addComponent(new Label("Choose the encoding method"));
            contentArea.addComponent(encryptionMethodsCombo);
            contentArea.addComponent(new Separator(Direction.HORIZONTAL).setPreferredSize(new TerminalSize(60, 1)));

//            contentArea.addComponent(new Label("Enter the path of original text file for encoding"));
//            contentArea.addComponent(pathToTextFileTxt);
//            contentArea.addComponent(new Separator(Direction.HORIZONTAL)).setPreferredSize(new TerminalSize(60, 1));

            contentArea.addComponent(new Button("Ok (Generate)", mainObj::doEncrypt));

            window.setComponent(contentArea);
            textGUI.addWindowAndWait(window);
        }
        finally
        {
            screen.stopScreen();
        }


    }
    public void doEncrypt()
    {
        EncryptionMethod method = EncryptionMethod.Caesar;
        switch (encryptionMethodsCombo.getText())
        {
            case "Hill" ->
            {
                method = EncryptionMethod.Hill;
                break;
            }
            case "Caesar" ->
            {
                break;
            }
            case "Vigenere" ->
            {
                method = EncryptionMethod.Vigenere;
                break;
            }
        }
        Generator generator = new Generator();
        Case[] cases = generator.generate(basePathTxt.getText(),
                pathToTextFileTxt.getText(),Integer.parseInt(countOfTestCases.getText()),
                Integer.parseInt(leastCharsPerPart.getText()), method);
        generator.writeToFile(cases, basePathTxt.getText(), encryptionMethodsCombo.getText());
    }

}

