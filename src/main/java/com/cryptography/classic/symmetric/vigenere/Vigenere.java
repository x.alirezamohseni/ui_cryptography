package com.cryptography.classic.symmetric.vigenere;

public class Vigenere
{
    public Vigenere()
    {

    }

    public static String encrypt(String message, String key)
    {
        key = new Vigenere().fitKeyToMessage(key.toLowerCase(), message.replace(" ", "").length());
        char keyCharArray[] = key.toCharArray();
        char messageCharArray[] = message.toCharArray();
        StringBuilder encryptedMessage = new StringBuilder();

        int countOfSpaces = 0;
        for (int i = 0; i < messageCharArray.length; i++)
        {

            if (messageCharArray[i] == ' ')
            {
                encryptedMessage.append(' ');
                ++countOfSpaces;
                continue;
            }

            int encKey = (int) keyCharArray[i - countOfSpaces] - 97;
            char encryptedChar = new Vigenere().encryptCharWithCaesar(Character.toLowerCase(messageCharArray[i]), encKey);
            encryptedMessage.append(Character.isUpperCase(messageCharArray[i]) ? Character.toUpperCase(encryptedChar) : encryptedChar);
        }
        return encryptedMessage.toString();
    }

    public static String decrypt(String encryptedMessage, String key)
    {
        key = new Vigenere().fitKeyToMessage(key.toLowerCase(), encryptedMessage.replace(" ", "").length());
        char keyCharArray[] = key.toCharArray();
        char messageCharArray[] = encryptedMessage.toCharArray();
        StringBuilder decryptedMessage = new StringBuilder();

        int countOfSpaces = 0;
        for (int i = 0; i < messageCharArray.length; i++)
        {

            if (messageCharArray[i] == ' ')
            {
                decryptedMessage.append(' ');
                ++countOfSpaces;
                continue;
            }

            int decKey = (int) keyCharArray[i - countOfSpaces] - 97;
            char decryptedChar = new Vigenere().decryptCharWithCaesar(Character.toLowerCase(messageCharArray[i]), decKey);
            decryptedMessage.append(Character.isUpperCase(messageCharArray[i]) ? Character.toUpperCase(decryptedChar) : decryptedChar);
        }

        return decryptedMessage.toString();
    }

    public char encryptCharWithCaesar(char character, int key)
    {
        int characterCode = (((character - 97) + key) % 26) + 97;
        return (char) characterCode;
    }

    public char decryptCharWithCaesar(char character, int key)
    {
        int characterCode = ((((character - 97) - key) + 26) % 26) + 97;
        return (char) characterCode;
    }

    public String fitKeyToMessage(String key, int lengthOfMessage)
    {
        StringBuilder expandingString = new StringBuilder();
        do
        {
            expandingString.append(key);
        }
        while (expandingString.length() <= lengthOfMessage);

        return expandingString.substring(0, lengthOfMessage).toString();
    }
}
