package com.cryptography.classic.symmetric.hill;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class HillMatrix
{
    private final int matrix[][];
    private final int size;

    public HillMatrix(int[][] matrix)
    {
        for (int row[] : matrix)
        {
            if (row.length != matrix.length) throw new IllegalArgumentException("The matrix should be in square form.");
        }
        this.matrix = matrix;
        this.size = matrix.length;
    }

    public int calculateDeterminant()
    {
        if (matrix.length == 1) return matrix[0][0];

        int det = 0;
        int convertedMatrix[][] = new int[0][];
        int rowForDet[] = matrix[0];

        for (int i = 0; i < rowForDet.length; i++)
        {
            int multiplier = ((int) Math.pow(-1, i)) * rowForDet[i];
            ArrayList<int[]> newMatrix = new ArrayList<>();
            for (int j = 1; j < matrix.length; j++)
            {
                ArrayList<Integer> newRow = new ArrayList<>();
                for (int k = 0; k < matrix[j].length; k++)
                {
                    if (k != i) newRow.add(matrix[j][k]);
                }
                int[] row = newRow.stream().mapToInt(x -> x).toArray();
                newMatrix.add(row);

            }

            convertedMatrix = new int[newMatrix.size()][newMatrix.size()];

            for (int j = 0; j < convertedMatrix.length; j++)
            {
                convertedMatrix[j] = newMatrix.get(j);
            }

            det += multiplier * new HillMatrix(convertedMatrix).calculateDeterminant();

        }

        return det;
    }

    public int[][] multiply(int second[][])
    {
        if (second.length != matrix[0].length)
            throw new IllegalArgumentException(String.format("The matrix for multiplication should have %d rows.", matrix[0].length));


        ArrayList<int[]> newMatrixRows = new ArrayList<>();
        for (int[] row : matrix)
        {
            int newRow[] = new int[second[0].length];
            for (int i = 0; i < second[0].length; i++)
            {
                int index = 0;
                for (int j = 0; j < row.length; j++)
                {
                    index += (row[j] * second[j][i]);
                }
                newRow[i] = index;
            }
            newMatrixRows.add(newRow);
        }
        int result[][] = new int[newMatrixRows.size()][];
        for (int i = 0; i < newMatrixRows.size(); i++)
        {
            result[i] = newMatrixRows.get(i);
        }
        return result;
    }
    
    public int[][] calculateTranspose()
    {
        int newMatrix[][] = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[0].length; j++)
            {
                newMatrix[j][i] = matrix[i][j];
            }
        }
        return newMatrix;
    }

    public int getSize()
    {
        return matrix.length;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < matrix.length; i++)
        {
            builder.append("[");
            for (int j = 0; j < matrix[i].length ; j++)
            {
                builder.append(matrix[i][j]);
                if (j != matrix[i].length -1 )
                    builder.append(", ");
            }
            builder.append("]\n");
        }
        return builder.toString();
    }
}
