package com.cryptography.classic.symmetric.hill;

import java.util.ArrayList;
import java.util.Arrays;

public class Hill
{
    public Hill()
    {

    }

    public static String encrypt(HillMatrix key, String message)
    {

        if (key.calculateDeterminant() == 0)
            throw new IllegalArgumentException("The determinant of key matrix is equals to 0. In this way encrypted message won't be decrypted!");

        StringBuilder encryptedMessage = new StringBuilder();

        int messageSizeForEncryption = key.getSize();

        if ((message.length() % messageSizeForEncryption) != 0)
        {
            char lastChar = message.charAt(message.length() - 1);
            StringBuilder messageBuilder = new StringBuilder(message);
            while ((messageBuilder.length() % messageSizeForEncryption) != 0)
            {
                messageBuilder.append(lastChar);
            }
            message = messageBuilder.toString();
        }
        char messageCharacters[] = message.toLowerCase().toCharArray();

        ArrayList<Character> charactersOfBlock = new ArrayList<>();
        ArrayList<char[]> blocks = new ArrayList<>();

        for (int i = 0; i < messageCharacters.length; i++)
        {
            charactersOfBlock.add(messageCharacters[i]);

            if ((i + 1) % messageSizeForEncryption == 0)
            {
                blocks.add(generateBlock(charactersOfBlock));
                charactersOfBlock.clear();
            }
        }

        for (char[] c : blocks)
        {
            int blockForMultiply[][] = new int[messageSizeForEncryption][1];
            for (int i = 0; i < c.length; i++)
            {
                blockForMultiply[i][0] = c[i] - 97;
                if (c[i] == ' ')
                    blockForMultiply[i][0] = 26;

            }
            int result[][] = key.multiply(blockForMultiply);

            int modulatedResult[][] = modulateRes(result);
            extractCharacters(modulatedResult, encryptedMessage);
        }


        return encryptedMessage.toString();
    }

    private static void extractCharacters(int[][] modulatedResult, StringBuilder encryptedMessage)
    {
        for (int i = 0; i < modulatedResult.length; i++)
        {
            for (int j = 0; j < modulatedResult[0].length; j++)
            {
                char encryptedCharacter = (char) (modulatedResult[i][j] + 97);
                encryptedMessage.append(encryptedCharacter);
            }
        }
    }

    private static int[][] modulateRes(int[][] matrix)
    {
        int res[][] = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++)
        {
            for (int j = 0; j < matrix[0].length; j++)
            {
                res[i][j] = (matrix[i][j]) % 26;
            }
        }
        return res;
    }

    private static char[] generateBlock(ArrayList<Character> charactersOfBlock)
    {
        StringBuilder collection = new StringBuilder(charactersOfBlock.size());
        for (char c : charactersOfBlock)
        {
            collection.append(c);
        }
        return collection.toString().toCharArray();
    }


}
