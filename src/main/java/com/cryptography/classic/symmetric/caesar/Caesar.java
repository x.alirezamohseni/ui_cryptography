package com.cryptography.classic.symmetric.caesar;

import java.util.Random;

public class Caesar
{
    private final Random random = new Random();
    private int key;

    public Caesar()
    {
        key = random.nextInt(25);
    }

    public Caesar(int seed)
    {
        random.setSeed(seed);
        key = random.nextInt(25);
    }

    public String encrypt(String message)
    {
        StringBuilder encryptedMessage = new StringBuilder();
        char messageCharacters[] = message.toCharArray();
        for (char messageCharacter : messageCharacters)
        {
            if (messageCharacter == ' ')
            {
                encryptedMessage.append(' ');
                continue;
            }

            int shiftToZero = Character.isUpperCase(messageCharacter) ? 65 : 97;

            int encryptedCharCode = (((messageCharacter - shiftToZero) + key) % 26) + shiftToZero;
            encryptedMessage.append((char) encryptedCharCode);
        }
        return encryptedMessage.toString();
    }

    public String decrypt(String encryptedMessage, int key)
    {
        StringBuilder decryptedMessage = new StringBuilder();
        char encMessageCharacters[] = encryptedMessage.toCharArray();
        for (char encMessageCharacter : encMessageCharacters)
        {
            if (encMessageCharacter == ' ')
            {
                decryptedMessage.append(' ');
                continue;
            }

            int shiftToZero = Character.isUpperCase(encMessageCharacter) ? 65 : 97;

            int decryptedCharCode = ((encMessageCharacter - shiftToZero) - key) >= 0 ?
                    ((encMessageCharacter - shiftToZero) - key) + shiftToZero :
                    (26 + ((encMessageCharacter - shiftToZero) - key)) + shiftToZero;
            decryptedMessage.append((char) decryptedCharCode);
        }
        return decryptedMessage.toString();
    }

    public String decrypt(String encryptedMessage)
    {
        StringBuilder decryptedMessage = new StringBuilder();
        char encMessageCharacters[] = encryptedMessage.toCharArray();
        for (char encMessageCharacter : encMessageCharacters)
        {
            if (encMessageCharacter == ' ')
            {
                decryptedMessage.append(' ');
                continue;
            }

            int shiftToZero = Character.isUpperCase(encMessageCharacter) ? 65 : 97;

            int decryptedCharCode = ((encMessageCharacter - shiftToZero) - key) >= 0 ?
                    ((encMessageCharacter - shiftToZero) - key) + shiftToZero :
                    (26 + ((encMessageCharacter - shiftToZero) - key)) + shiftToZero;
            decryptedMessage.append((char) decryptedCharCode);
        }
        return decryptedMessage.toString();
    }

    public void setNewRandomKey()
    {
        key = random.nextInt(25);
    }

    public void setNewRandomKey(int seed)
    {
        random.setSeed(seed);
        key = random.nextInt(25);
    }

    public int getKey()
    {
        return key;
    }

    public void setKey(int key)
    {
        if (key > 25 || key < 0) throw new IllegalArgumentException("The key should be between 0 an 25.");
        this.key = key;
    }
}
