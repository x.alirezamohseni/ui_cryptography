
import static org.junit.jupiter.api.Assertions.*;

import com.cryptography.classic.symmetric.hill.Hill;
import com.cryptography.classic.symmetric.hill.HillMatrix;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class TestHill
{
    @Test
    @Order(1)
    @DisplayName("Testing encryption ....")
    public void testEncryption()
    {

        assertEquals(
                Hill.encrypt(new HillMatrix(
                        new int[][]{
                                {6, 24, 1},
                                {13, 16, 10},
                                {20, 17, 15}
                        }),
                        "ACT"), "poh");

        assertEquals(
                Hill.encrypt(new HillMatrix(
                        new int[][]{
                                {11, 8},
                                {3, 7}
                        }), "letusfly"), "xjfpelbt");

    }
}
