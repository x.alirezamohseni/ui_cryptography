import static org.junit.jupiter.api.Assertions.*;

import com.cryptography.classic.symmetric.hill.Hill;
import com.cryptography.classic.symmetric.hill.HillMatrix;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class TestHillMatrix
{
    @Test
    @Order(1)
    @DisplayName("Testing for good determinant calculation ....")
    public void testMatrixDeterminant()
    {
        assertEquals(new HillMatrix(
                new int[][]{
                        {9, 1, 6},
                        {1, 9, 1},
                        {6, 1, 9}
                }).calculateDeterminant(), 399);

        assertEquals(new HillMatrix(
                new int[][]{
                        {213, 40},
                        {53, 6}
                }).calculateDeterminant(), -842);

        assertEquals(new HillMatrix(
                new int[][]{
                        {7, 1, 8, 4},
                        {9, 2, 9, 3},
                        {7, 3, 8, 2},
                        {4, 1, 9, 1}
                }).calculateDeterminant(), -104);

        assertEquals(new HillMatrix(
                new int[][]{
                        {1, 1, 0},
                        {3, 3, 2},
                        {2, 2, 3}
                }).calculateDeterminant(), 0);

    }

    @Test
    @Order(2)
    @DisplayName("Testing matrix creation ....")
    public void testMatrixCreationSquareFormCheck()
    {
        assertThrows(IllegalArgumentException.class,
                () -> new HillMatrix(new int[][]{
                        {1, 1, 2},
                        {2, 1},
                        {2, 4, 4}
                }));

        assertDoesNotThrow(
                () -> new HillMatrix(new int[][]{
                        {1, 1, 2},
                        {2, 1, 5},
                        {2, 4, 4}
                }));

        assertThrows(IllegalArgumentException.class,
                () -> new HillMatrix(new int[][]{
                        {1, 1, 2},
                        {2},
                        {2, 4}
                }));
        assertThrows(IllegalArgumentException.class,
                () -> new HillMatrix(new int[][]{
                        {1, 1, 2},
                        {2, 1, 6},
                        {2, 4}
                }));
        assertThrows(IllegalArgumentException.class,
                () -> new HillMatrix(new int[][]{
                        {1, 1, 2},
                        {2, 1, 3, 4},
                        {2, 4, 4}
                }));
        assertDoesNotThrow(
                () -> new HillMatrix(new int[][]{
                        {1, 1, 2, 6},
                        {2, 1, 3, 4},
                        {2, 4, 4, 5},
                        {2, 4, 4, 5}
                }));
    }

    @Test
    @Order(3)
    @DisplayName("Testing matrix multiplication ....")
    public void testMatrixMultiplication()
    {
        assertEquals(Arrays.deepToString(new HillMatrix(
                new int[][]{
                        {9, 1, 6},
                        {1, 9, 1},
                        {6, 1, 9}
                }).multiply(
                new int[][]{
                        {9, 4, 6},
                        {4, 9, 4},
                        {6, 4, 9}
                })
        ), "[[121, 69, 112], [51, 89, 51], [112, 69, 121]]");

        assertEquals(Arrays.deepToString(new HillMatrix(
                new int[][]{
                        {9, 1, 6},
                        {1, 9, 1},
                        {6, 1, 9}
                }).multiply(
                new int[][]{
                        {9},
                        {4},
                        {6}
                })
        ), "[[121], [51], [112]]");

        assertEquals(Arrays.deepToString(new HillMatrix(
                new int[][]{
                        {5, 1},
                        {2, 9}
                }).multiply(
                new int[][]{
                        {9, 4},
                        {4, 9}
                })
        ), "[[49, 29], [54, 89]]");

        assertEquals(Arrays.deepToString(new HillMatrix(
                new int[][]{
                        {5}
                }).multiply(
                new int[][]{
                        {7}
                })
        ), "[[35]]");
    }

    @Test
    @Order(4)
    @DisplayName("Testing transpose calculation ....")
    public void testTranspose()
    {

        assertEquals(Arrays.deepToString(new HillMatrix(
                        new int[][]{
                                {6, 24, 1},
                                {13, 16, 10},
                                {20, 17, 15}
                        }).calculateTranspose())
                , "[[6, 13, 20], [24, 16, 17], [1, 10, 15]]");

        assertEquals(Arrays.deepToString(new HillMatrix(
                        new int[][]{
                                {9, 1, 6},
                                {1, 9, 1},
                                {6, 1, 9}
                        }).calculateTranspose())
                , "[[9, 1, 6], [1, 9, 1], [6, 1, 9]]");

        assertEquals(Arrays.deepToString(new HillMatrix(
                        new int[][]{
                                {1, 1, 2, 6},
                                {2, 1, 3, 4},
                                {2, 4, 4, 5},
                                {2, 4, 4, 5}
                        }).calculateTranspose())
                , "[[1, 2, 2, 2], [1, 1, 4, 4], [2, 3, 4, 4], [6, 4, 5, 5]]");


    }

}
