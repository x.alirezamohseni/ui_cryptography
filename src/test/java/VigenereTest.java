import com.cryptography.classic.symmetric.vigenere.Vigenere;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VigenereTest
{
    Vigenere testVigenere = new Vigenere();

    @Test
    @Order(3)
    @DisplayName("Testing encryption ....")
    public void testEnc()
    {
        assertEquals(Vigenere.encrypt("ATTACKATDAWN", "LEMONLEMONLE"), "LXFOPVEFRNHR");
        assertEquals(Vigenere.encrypt("Attack on Titan", "erenyeager"), "Ekxnao ot Xzxrr");
        assertEquals(Vigenere.encrypt("The best thing about human beings is that they stack so neatly"
                , "frankunderwood"), "Yye oomg wlzju oetlt uegnq fvebuv nj tukn gkip ohofp jo aougoc");
        assertEquals(Vigenere.encrypt("AliReza Mohseni", "myname"), "MjvRqdm Kbheizg");
    }

    @Test
    @Order(4)
    @DisplayName("Testing decryption ....")
    public void testDec()
    {
        assertEquals(Vigenere.decrypt("LXFOPVEFRNHR", "LEMONLEMONLE"), "ATTACKATDAWN");
        assertEquals(Vigenere.decrypt("Ekxnao ot Xzxrr", "erenyeager"), "Attack on Titan");
        assertEquals(Vigenere.decrypt("Yye oomg wlzju oetlt uegnq fvebuv nj tukn gkip ohofp jo aougoc"
                , "frankunderwood"), "The best thing about human beings is that they stack so neatly");
        assertEquals(Vigenere.decrypt("MjvRqdm Kbheizg", "myname"), "AliReza Mohseni");
    }

    @Test
    @Order(1)
    @DisplayName("Testing for fit key to message ....")
    public void testFitKeyToMessage()
    {
        assertEquals(testVigenere.fitKeyToMessage("hello", 20), "hellohellohellohello");
        assertEquals(testVigenere.fitKeyToMessage("HollA", 42), "HollAHollAHollAHollAHollAHollAHollAHollAHo");
        assertEquals(testVigenere.fitKeyToMessage("nabla", 16), "nablanablanablan");
        assertEquals(testVigenere.fitKeyToMessage("THEGRATECYRUS", 48), "THEGRATECYRUSTHEGRATECYRUSTHEGRATECYRUSTHEGRATEC");
        assertEquals(testVigenere.fitKeyToMessage("ONLYGODCANJUDJEME", 60), "ONLYGODCANJUDJEMEONLYGODCANJUDJEMEONLYGODCANJUDJEMEONLYGODCA");
    }


    @Test
    @Order(2)
    @DisplayName("Testing for character encryption with Caesar ....")
    public void testCharEncode()
    {
        assertEquals(testVigenere.encryptCharWithCaesar('a', 2), 'c');
        assertEquals(testVigenere.encryptCharWithCaesar('x', 3), 'a');
        assertEquals(testVigenere.encryptCharWithCaesar('b', 23), 'y');
        assertEquals(testVigenere.encryptCharWithCaesar('z', 8), 'h');
        assertEquals(testVigenere.encryptCharWithCaesar('h', 10), 'r');
    }

}
